/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejerciciomaterias;

import java.util.Scanner;

/**
 *
 * @author Luisa
 */
public class EjercicioMaterias {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner Leer=new Scanner (System.in);
        Double NotaAlumno[][]=new Double [4][4];
        String Datos[][]=new String [128][128];
        
        String Nombre="", NombreMatMin="", NombreFisMin="", NombreFisMax="";
        Double DefMatCurso,MatCurso=0.0,FisCurso=0.0, DefFisCurso, NFisMax=0.0, NFisMin=5.0,Fisica, P1,P2,PT,PQ,P4,TP1, TP2,TP3,TP4,TQ,TT, NotaT=0.0, NotaQ=0.0, Definitiva, Matematicas, NMat=0.0, NMatMin=5.0;
        for (int i=0;i<2;i++)
        {
                
            System.out.println("ESTUDIANTE ["+(i+1)+"]");
            System.out.println("Nombre del alumno  ");
            Datos[i][0]=Leer.next();
            System.out.println("Apellido del alumno ");
            Datos[i][1]=Leer.next();
            System.out.println("Ingrese la identificacion ");
            Datos[i][2]=Leer.next();
            System.out.println("Ingrese la edad ");
            Datos[i][3]=Leer.next();
            System.out.println("La materia 1 corresponde a Matematicas, la materia 2 corresponde a Fisica, la materia 3 corresponde a Quimica y la materia 4 a Programacion");
            for (int j=0;j<2;j++)
            {
                               
                System.out.println("Materia ["+(j+1)+"] ");
                System.out.println("Ingrese la nota P1");
                P1=Leer.nextDouble();
                TP1=P1*0.25;
                System.out.println("Ingrese la nota P2");
                P2=Leer.nextDouble();
                TP2=P2*0.25;
                System.out.println("Notas P3");
                for (int z=0;z<7;z++)
                {
                    System.out.println("Ingrese la nota del trabajo ["+(z+1)+"] ");
                    PT=Leer.nextDouble();
                    NotaT=NotaT+PT;
                }
                TT=NotaT/7;
                for (int k=0;k<5;k++)
                {
                    System.out.println("Ingrese la nota del quiz ["+(k+1)+"] ");
                    PQ=Leer.nextDouble();
                    NotaQ=NotaQ+PQ;               
                }
                TQ=NotaQ/5;
                TP3=((TT+TQ)/2)*0.20;
                System.out.println("Por favor ingrese la nota P4");
                P4=Leer.nextDouble();
                TP4=P4*0.30;
                Definitiva=TP1+TP2+TP3+TP4;
                NotaAlumno[i][j]=Definitiva;
                NotaT=0.0;
                NotaQ=0.0;
                Matematicas=NotaAlumno[i][0];
                
                /* MATEMATICAS */
                if (NMat<Matematicas)
                {
                    NMat=Matematicas;
                    Nombre=Datos[i][0];
                }
                else 
                {
                    if (NMatMin>Matematicas)
                    {
                        NMatMin=Matematicas;
                        NombreMatMin=Datos[i][0];
                    }
                }
            }
            Fisica=NotaAlumno[i][1];
            /*FISICA*/
             if (NFisMax<Fisica)
             {
                    NFisMax=Fisica;
                    NombreFisMax=Datos[i][1];
            }
            else 
            {
                if (NFisMin>Fisica)
                {
                    NFisMin=Fisica;
                    NombreFisMin=Datos[i][1];
                }
            }
            MatCurso=MatCurso+NotaAlumno[i][0];
            FisCurso=FisCurso+NotaAlumno[i][1];
        }
        /* DEFINITIVA DE MATERIA POR CURSO */
        DefMatCurso=MatCurso/2;
        DefFisCurso=FisCurso/2;
        for (int i =0;i<2;i++)
        {
            for (int s=0;s<4;s++)
            {
                System.out.print(Datos[i][s]+" ");
            }
            System.out.println("");

            for (int j=0;j<2;j++)
            {
                System.out.print(NotaAlumno[i][j]+" ");
            }
            System.out.println("");
            
        }
        System.out.println("MAYOR DEFINITIVA DEL CURSO EN MATEMATICAS.");
        System.out.println("Nota: "+NMat +" Nombre: "+Nombre);
        System.out.println("MENOR DEFINITIVA DEL CURSO EN MATEMATICAS. ");
        System.out.println("Nota: "+NMatMin +" Nombre: "+NombreMatMin);
        System.out.println("PROMEDIO DE MATEMATICAS DEL CURSO: "+ DefMatCurso);
        System.out.println("MAYOR DEFINITIVA DEL CURSO EN FISICA.");
        System.out.println("Nota: "+NFisMax +" Nombre: "+NombreFisMax);
        System.out.println("MENOR DEFINITIVA DEL CURSO EN FISICA. ");
        System.out.println("Nota: "+NFisMin +" Nombre: "+NombreFisMin);
        System.out.println("PROMEDIO DE FISICA DEL CURSO: "+ DefFisCurso);
    }
}

    

